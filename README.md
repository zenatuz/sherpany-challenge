# Hosts: 
- 185.79.233.182  kube-master
- 185.79.233.91   kube-node-01
- 185.79.233.238  kube-node-02

# Steps:

## 1 - Install docker engine to run k8s
https://docs.docker.com/engine/install/ubuntu/

## 2 - configure runtime
https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker

## 3 - Install kubeadm packages
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

## 4 - Create internal dns for each host know each other (/etc/hosts/)
185.79.233.182  kube-master
185.79.233.91   kube-node-01
185.79.233.238  kube-node-02

## 5 - Disable SWAP
Swap wasn't enable on the linux, so no action on this topic.

## 6 - init cluster com `kubeadm init --pod-network-cidr=10.244.0.0/16`

After init the cluster, the command above will provide this output:

```shell
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 185.79.233.182:6443 --token pgo9sl.0ehg1y5tp7fw8y20 \
    --discovery-token-ca-cert-hash sha256:94078e2490b38f48f231e88e40f024ae34adce8c8ccee67ba3877bb2fc900e29
```

## 7 - run kubeadm join on nodes to add them to cluster.

Now, the following command will add the nodes to the fresh cluster.

`kubeadm join 185.79.233.182:6443 --token pgo9sl.0ehg1y5tp7fw8y20 --discovery-token-ca-cert-hash sha256:94078e2490b38f48f231e88e40f024ae34adce8c8ccee67ba3877bb2fc900e29`

				
## 8 - Install a pod network addon, I choose Calico 
Follow the instructions on https://docs.projectcalico.org/getting-started/kubernetes/quickstart

> Note: I needed to change the CIDR address on custom-resources.yaml

After install it, the nodes will became available after a few moments:

```shell
root@kube-master:/home/ubuntu# kubectl get nodes
NAME           STATUS   ROLES                  AGE     VERSION
kube-master    Ready    control-plane,master   3m59s   v1.20.0
kube-node-01   Ready    <none>                 2m47s   v1.20.0
kube-node-02   Ready    <none>                 2m43s   v1.20.0
```

## 9 - deploy ingress 

Follow the instructions on: https://kubernetes.github.io/ingress-nginx/deploy/#bare-metal

As this cloud provider doesn't have the load balancer option, I used the nodeport. After that I needed to to configure the nodes IPs on the ingress service.

  ```yaml
  externalIPs:
  - 185.79.233.91
  - 185.79.233.238
  ```
  
## 10 - install metrics server
Follow the instructions on: https://github.com/kubernetes-sigs/metrics-server

It was necessary to configure this parameter to startup the metrics-service

```yaml
- --kubelet-insecure-tls #https://stackoverflow.com/questions/59969791/metrics-server-not-working-in-kubernetes-cluster
```

## 11 - log stack
I coudn't make the storage plugin works even following the documentation on https://github.com/cloudscale-ch/csi-cloudscale

## 12 - CICD
I choose to use the gitlab platform on this link: https://gitlab.com/zenatuz/sherpany-challenge.

I had some issues with continuous deploy regarding the docker image with kubectl and credentials. 
But the pipeline configuration can be viewed at: https://gitlab.com/zenatuz/sherpany-challenge/-/pipelines

## 13 - Accessing the application
The example application can be accessed at: http://playground.renatobatista.com.br/




